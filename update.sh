#!/bin/bash

cd "$( dirname "${BASH_SOURCE[0]}" )"

echo -n "" > lastlog

clExit()
{
  [ "x${1}" == "x0" ] ||
  {
    # Print script name, host name and error code
    echo -n "${BASH_SOURCE[0]} FAILED on $HOSTNAME with ${1}"
    
    # If exists, print error message
    [ "x${2}" == "x" ] && echo "" || echo ": ${2}"
    
    # Print log
    cat lastlog
    
  # Everything to stderr
  } >&2
  
  # Exit with errorcode $1
  exit ${1}
}

# Find all zone files
prunedFind()
{
  find . \( -path "./.git" -prune -or ! -path "./update.sh" -and ! -path "./lastlog" \) -type f -print
}

# Query DNS, exit nicely on error
myHost()
{
  # egrep -v returns 1 when regexp is found, AKA, dig returns no result or error (which begins with ';')
  dig "${1}" +short | egrep -v '^$|^;' || clExit 1 "Failed DNS lookup for '${1}'"
}

# Get interface IP, $1 interface name
getInterfaceIpv4()
{
	interfaceIPv4="$(ip address show dev ${1} | grep 'inet ' | awk '{print $2}' | awk -F/ '{print $1}')"
	[ "$(echo ${interfaceIPv4} | wc -l)X" == "1X" ] || clExit 1 "getInterfaceIpv4 ${@} failed: ${interfaceIPv4}"
	
	# It is not exact ipv4 match so something like 999.999.999.999 can pass but this check is enough for us
	echo ${interfaceIPv4} | grep -x -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' || clExit 1 "getInterfaceIpv4 ${@} failed: ${interfaceIPv4}"
}

#############################################
# IPs
#
# All IPs into an associative array
declare -A ALL_IPS

# Get Cooperative Datacenter dynamic IP or die
ALL_IPS[MOVISTAR_IP]="$(getInterfaceIpv4 ppp0)" || exit $?
ALL_IPS[ORANGE_IP]="$(getInterfaceIpv4 pporange.832)" || exit $?
ALL_IPS[IPREDATOR_IP]="$(getInterfaceIpv4 ppredator)" || exit $?
ALL_IPS[ADA_IP]="10.1.152.27"
ALL_IPS[PEX_RELAY_IP]="10.1.152.16"
ALL_IPS[JRI_IP]="10.1.152.15"
#
# End IPs
###########################################

# MD5 result to string into a variable (no tmpfile)
MD5SUMS="$( prunedFind | xargs md5sum )"

# Update from GIT
git fetch --all &>> lastlog
git reset --hard origin/master &>> lastlog

export PATH=${PATH}:/usr/sbin/

ipset add -exist opub4 ${ALL_IPS[MOVISTAR_IP]}
ipset add -exist opub4 ${ALL_IPS[ORANGE_IP]}
ipset add -exist opub4 ${ALL_IPS[IPREDATOR_IP]}

# Search and replace all IPs from our associative array on all zone files
for VAR in "${!ALL_IPS[@]}"
do
   VALUE="${ALL_IPS[$VAR]}"
   prunedFind | xargs sed -i "s/\$${VAR}/${VALUE}/g"
done

# If any change detected apply and restart iptables
echo "${MD5SUMS}" | md5sum --status --check - ||
{
	cp rules-save /var/lib/iptables/rules-save
	/etc/init.d/iptables restart
}

clExit 0
